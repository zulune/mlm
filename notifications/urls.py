from django.conf.urls import url

from .views import (
    FeedbackView,
    NotificationView
)

urlpatterns = [
    url(r'^feedback/$', FeedbackView.as_view(), name='feedback_view'),
    url(r'^mail_notifications/$', NotificationView.as_view(), name="notification_view"),
]
