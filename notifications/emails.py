from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives,send_mass_mail
from django.template.loader import render_to_string

from account.models import Profile

def send_feedback_email(full_name, email, message):
    c = {'full_name': full_name, 'email': email, 'message': message}

    email_subject = render_to_string(
        'notifications/email/feedback_email_subject.txt', c
    ).replace('\n', '')
    email_body = render_to_string(
        'notifications/email/feedback_email_message.txt', c
    )
    html_content = email_body
    email = EmailMultiAlternatives(
        email_subject, email_body, email,
        [settings.DEFAULT_FROM_EMAIL], [settings.EMAIL_HOST_USER],
        headers={'Reply-to': email}
    )
    email.attach_alternative(html_content, "text/html")
    return email.send(fail_silently=False)

def send_notification_email_task(subject, message):
    recepient = get_receiver_user()
    email = []
    for r in recepient:
        msg = (subject, message, settings.DEFAULT_FROM_EMAIL, [r])
        email.append(msg)
    return send_mass_mail(email, fail_silently=False)


def get_receiver_user():
    email_list = Profile.objects.get_receiver()
    return email_list
