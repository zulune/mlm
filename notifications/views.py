from django.shortcuts import render
from django.views.generic import FormView
from django.contrib.messages.views import SuccessMessageMixin

from .forms import FeedbackForm, NotificationForm
# Create your views here.


class FeedbackView(SuccessMessageMixin, FormView):
    template_name = 'notifications/contact.html'
    form_class = FeedbackForm
    success_url = '/'
    success_message = 'Ваше письмо успешно отправлено!'

    def form_valid(self, form):
        form.send_email()
        return super(FeedbackView, self).form_valid(form)


class NotificationView(SuccessMessageMixin, FormView):
    template_name = 'notifications/notification_view.html'
    form_class = NotificationForm
    success_url = '/'
    success_message = 'Ваше письмо успешно отправлено!'

    def form_valid(self, form):
        form.send_notification_email()
        return super(FeedbackView, self).form_valid(form)
