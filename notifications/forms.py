# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _

from .tasks import send_feedback_email_task, send_notification_email_task

################## Code here ####################

class FeedbackForm(forms.Form):
    full_name   = forms.CharField(label=_('Полное имя '))
    email       = forms.EmailField(label=_('Email'))
    message     = forms.CharField(
        label=_('Messages'),
        widget=forms.Textarea(attrs={'rows': 5})
    )
    honeypot    = forms.CharField(widget=forms.HiddenInput(), required=False)

    def send_email(self):
        if self.cleaned_data['honeypot']:
            return False
        send_feedback_email_task.delay(
            self.cleaned_data['full_name'],
            self.cleaned_data['email'],
            self.cleaned_data['message']
        )

class NotificationForm(forms.Form):
    subject = forms.CharField(label=_('Тема'))
    message = forms.CharField(label=_('Messages'), widget=forms.Textarea)

    def send_notification_email(self):
        subject = self.cleaned_data['subject']
        message = self.cleaned_data['message']
        send_notification_email_task.delay(
            subject, message
        )
