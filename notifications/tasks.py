from celery.decorators import task
from celery.utils.log import get_task_logger

from .emails import send_feedback_email

logger = get_task_logger(__name__)

@task(name='send_feedback_email_task')
def send_feedback_email_task(full_name, email, message):
    logger.info("Sent feedback email")
    return send_feedback_email(full_name, email, message)


@task(name='send_notification_email_task')
def send_notification_email_task(subject, message):
    logger.info("Sent notifications email")
    return send_notification_email_task(subject, message)
