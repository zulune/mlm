User-agent: *
Disallow: /admin/
Disallow: /accoutns/profile
Disallow: /accoutns/logout
Disallow: /accoutns/login
Disallow: /accoutns/register
Disallow: /accoutns/settings
Disallow: /company_registration
Disallow: /banners_registration

Allow: /media/company_img
