from django.contrib import admin
from .models import Company, Category, Plan, Messages, Message_for_payment

# Register your models here.


class Message_for_paymentInlines(admin.TabularInline):
	model = Message_for_payment
	extra = 1



class CompanyAdmin(admin.ModelAdmin):
	search_fields = ('user', 'company_name', 'timestamp')
	list_display = ('user', 'plan', 'category', 'company_name', 'status', 'draft')
	list_filter = ('timestamp', 'update')
	# prepopulated_fields = {'slug': ('company_name', )}
	inlines = (Message_for_paymentInlines, )


	class Meta:
		model = Company


class CompanyPlan(admin.ModelAdmin):
	list_display = ('name', 'price', 'contribution')


class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name', )


class MessagePaymentAdmin(admin.ModelAdmin):
	list_display = ('company', 'value', 'timestamp')
	search_fields = ('company', 'timestamp')

	class Meta:
		model = Message_for_payment

admin.site.register(Category, CategoryAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Plan, CompanyPlan)

admin.site.register(Messages)
admin.site.register(Message_for_payment, MessagePaymentAdmin)
