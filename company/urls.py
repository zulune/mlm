from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^home/', index, name='index'),
    url(r'^company/$', company_all, name="company_all"),
	url(r'^company_detail/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', company_detail, name='company_detail'),
	url(r'^company_filter/(?P<id>[?a-z=A-Z0-9_.-]*)/$', company_filter, name='company_filter'),
	url(r'^company/(?P<status>[?a-z=A-Z0-9_.-]*)/$', status_filter, name='status_filter'),
	url(r'^listing/(?P<id>[?a-z=A-Z0-9_.-]*)/$', listing, name='listing'),
	url(r'^category/(?P<id>[?a-z=A-Z0-9_.-]*)/(?P<status>[?a-z=A-Z0-9_.-]*)/$', category_filter, name='category_filter'),
	url(r'^company_registration/$', company_registration, name='company_registration'),
	url(r'^wizzard_company/$', wizzard_company, name="wizzard_company"),
	url(r'^company/error/$', wizzard_pay_error, name='wizzard_pay_error'),
    url(r'^scam/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', scam, name='scam'),
]
