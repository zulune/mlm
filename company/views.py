# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q, Max

from .models import Company, status_company, Category
from .forms import CompanyRegistrationForm

from django.template import RequestContext
from django.core.mail import send_mail, send_mass_mail

from company.models import *
from comment.forms import CommentForm
from comment.models import Comment
from banners.models import *
# Create your views here.




def index(request):
	premium = premium_value()
	try:
		premium_plan = Plan.objects.get(price=premium)
		company_max = Company.objects.active().filter(plan_id=premium_plan.id).exclude(status='close')[:3]
	except:
		premium_plan = None
		company_max = None
	company_active = Company.objects.active().filter(status='active')[:12]
	company_expectation = Company.objects.filter(status='expectation').order_by('-timestamp')[:3]
	company_close = Company.objects.filter(status='close').order_by('-timestamp')[:3]

	template_data = {
		'company_max': company_max,
		'company_active': company_active,
		'company_expectation': company_expectation,
		'company_close': company_close,
		'premium_plan': premium_plan
	}
	return render(request, 'company/home.html', template_data)


def premium_value():
	all_plans = Plan.objects.all()
	premium = 0
	for p in all_plans:
		if p.price > premium:
			premium = p.price
	return premium


def company_all(request):
	company_list = Company.objects.active().exclude(status='close')
	paginator = Paginator(company_list, 10)
	page = request.GET.get("page")
	try:
		company = paginator.page(page)
	except PageNotAnInteger:
		company = paginator.page(1)
	except EmptyPage:
		company = paginator.page(paginator.num_pages)
	template_data = {'company': company}
	return render(request, 'company/company_all.html', template_data)


def company_detail(request, slug):
	single = Company.objects.get(slug=slug)
	payment = Message_for_payment.objects.filter(company_id=single.id)
	comments = single.comments
	initial_data = {
		'content_type': single.get_content_type,
		'object_id': single.id
	}
	form = CommentForm(request.POST or None, initial=initial_data)
	if form.is_valid():
		c_type = form.cleaned_data.get("content_type")
		content_type = ContentType.objects.get(model=c_type)
		obj_id = form.cleaned_data.get("object_id")
		content_data = form.cleaned_data.get("content")
		parent_obj = None
		try:
			parent_id = int(request.POST.get('parent_id'))
		except:
			parent_id = None

		if parent_id:
			parent_qs = Comment.objects.filter(id=parent_id)
			if parent_qs.exists() and parent_qs.count() == 1:
				parent_obj = parent_qs.first()

		new_comment, created = Comment.objects.get_or_create(
			user = request.user,
			content_type = content_type,
			object_id=obj_id,
			content = content_data,
			parent = parent_obj
		)
		print('all ready!!!')
		return HttpResponseRedirect("/company_detail/%s/" % single.slug)

	template_data = {'single': single, 'comment_form': form, 'comment_list': comments, 'payment_sing': payment}
	return render(request, 'company/company_detail.html', template_data)


@login_required
def scam(request, slug):
	single = Company.objects.get(slug=slug)
	if request.POST:
		user = request.user
		company = single
		message = request.POST.get('message')
		email_list = admin_email()
		messages = Messages.objects.get_or_create(
				user=user,
				company=company,
				message=message
			)
		send_mail('Scam', message, 'mlmconnect01@gmail.com', email_list)
		return HttpResponseRedirect('/company_detail/%s' % single.slug)
	template_data = {'single': single}
	return render(request, 'company/scam.html', template_data)



@login_required
def company_registration(request):
	plan = Plan.objects.all().order_by('-price')
	if request.POST:
		form = CompanyRegistrationForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			company_form = form.save(commit=False)
			company_form.user = request.user
			company_form.save()
			return HttpResponseRedirect('/')
	else:
		form = CompanyRegistrationForm()
	template_data = {'form': form, 'plan': plan}
	return render(request, 'company/registration_company.html', template_data)


def wizzard_company(request):
	name = "Pay for this"
	template_data = {'name': name}
	return render(request, 'company/wizzard_company.html')


def wizzard_pay_error(request):
	template_data = {}
	return render(request, 'company/pay_error.html', template_data)


def company_filter(request, id):
	name = Category.objects.get(id=id)
	company_filter = Company.objects.active().filter(category_id=id)
	template_data = {'company_filter': company_filter, "name": name}
	return render(request, 'company/filter.html', template_data)


def status_filter(request, status):
	company_list = Company.objects.active().filter(status=status)

	paginator = Paginator(company_list, 10)
	page = request.GET.get("page")
	try:
		company_filter = paginator.page(page)
	except PageNotAnInteger:
		company_filter = paginator.page(1)
	except EmptyPage:
		company_filter = paginator.page(paginator.num_pages)

	if status == 'active':
		name = "Платит"
		tag = "fa fa-money text-success"
	elif status == 'expectation':
		name = "Ожидает"
		tag = "fa fa-clock-o text-info"
	elif status == 'delays':
		name = "Задержывает"
		tag = "fa fa-money  text-warning"
	else:
		name = "Scam"
		tag = "fa fa-trash-o text-danger"
	template_data = {"company_filter": company_filter, "name": name}
	return render(request, 'company/status_filter.html', template_data)


def category_filter(request, id, status):
	if status == 'active':
		name = "Платит"
		tag = "fa fa-money text-success"
	elif status == 'expectation':
		name = "Ожидает"
		tag = "fa fa-clock-o text-info"
	elif status == 'delays':
		name = "Задержывает"
		tag = "fa fa-money  text-warning"
	else:
		name = "Scam"
		tag = "fa fa-trash-o text-danger"
	company_list = Company.objects.active().filter(category=id).filter(status=status)

	paginator = Paginator(company_list, 9)
	page = request.GET.get("page")
	try:
		cat_filter = paginator.page(page)
	except PageNotAnInteger:
		cat_filter = paginator.page(1)
	except EmptyPage:
		cat_filter = paginator.page(paginator.num_pages)

	template_data = {'cat_filter': cat_filter, 'name': name, 'tag': tag}
	return render(request, 'company/category_filter.html', template_data)


def listing(request, id):
	name = Plan.objects.get(id=id)
	company_list = Company.objects.active().filter(plan_id=id)

	paginator = Paginator(company_list, 10)
	page = request.GET.get("page")
	try:
		company_filter = paginator.page(page)
	except PageNotAnInteger:
		company_filter = paginator.page(1)
	except EmptyPage:
		company_filter = paginator.page(paginator.num_pages)

	template_data = {'company_filter': company_filter, 'name': name}
	return render(request, 'company/listing.html', template_data)


def search(request):
	if "q" in request.GET and request.GET["q"]:
		q = request.GET["q"]
		company_list = Company.objects.filter(company_name__icontains=q)

		paginator = Paginator(company_list, 10)
		page = request.GET.get("page")
		try:
			company_search = paginator.page(page)
		except PageNotAnInteger:
			company_search = paginator.page(1)
		except EmptyPage:
			company_search = paginator.page(paginator.num_pages)

		template_data = {"company_search": company_search, "query": q}
		return render(request, "company/result.html", template_data)
