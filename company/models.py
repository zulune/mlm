# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from django.contrib.sitemaps import Sitemap

from django.utils.text import slugify
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.safestring import mark_safe

from comment.models import Comment
from markdown_deux import markdown

import datetime
import time
# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=50)
	slug = models.SlugField(blank=True, null=True)
	class Meta:
		verbose_name_plural = _('Категории')

	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.name

status_company = (
		('active', u'Платит'),
		('delays', u'Задержывает'),
		('expectation', u'Ожидает'),
		('close', u'СКАМ')
	)


plan = (
		('base', 'Base'),
		('vip', 'Vip'),
		('premium', 'Premium')
	)

publish_status = (
		('draft',u'На проверке'),
		('publish',u'Опубликовано')
	)


class Plan(models.Model):
	name = models.CharField(max_length=100, verbose_name="Название плана")
	description = models.TextField(blank=True, null=True)
	price = models.PositiveIntegerField(verbose_name="Цена")
	contribution = models.PositiveIntegerField(verbose_name="Вклад")
	post = models.CharField(max_length=100, verbose_name="Количество постингов")
	banner = models.BooleanField(default=False, verbose_name="Баннер в шапке")
	banner2 = models.BooleanField(default=False, verbose_name="Баннер в центре")
	ad_instagram = models.BooleanField(default=False, verbose_name="Реклама в Instagram")
	ad_facebook = models.BooleanField(default=False, verbose_name="Реклама в Facebook")
	email_mass_message = models.BooleanField(default=False, verbose_name="Email рассылка")

	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = 'Тарифные планы'


class CompanyManager(models.Manager):
	def active(self, *args, **kwargs):
		return super(CompanyManager, self).filter(draft='publish').order_by("-timestamp")

	# def filter(self, *args, **kwargs):
	# 	return super(CompanyManager, self).filter(draft=True)


def upload_location(instance, filename):
    #filebase, extension = filename.split(".")
    #return "%s/%s.%s" %(instance.id, instance.id, extension)
    PostModel = instance.__class__
    new_id = PostModel.objects.order_by("id").last().id + 1

    return "%s/%s" %(new_id, filename)


class Company(models.Model):
	user = models.ForeignKey(User, verbose_name="Пользыватель")
	category = models.ForeignKey(Category, verbose_name="Категория", blank=True,null=True)
	company_name = models.CharField(max_length=50, verbose_name="Имя компании")
	slug = models.SlugField(unique=True)
	company_url = models.URLField(verbose_name="Сылка на проект")
	referal_url = models.URLField(verbose_name="Реферальная сылка", blank=True)
	referal_program = models.CharField(max_length=100, verbose_name="Реферальная программа")
	min_value = models.CharField(max_length=50, verbose_name="Минимальный вклад")
	max_value = models.CharField(max_length=50, verbose_name="Максимальный вклад")
	type_payments = models.CharField(max_length=50, verbose_name="Тип выплат")
	plan = models.ForeignKey(Plan, verbose_name="Тарифный план")
	status = models.CharField(choices=status_company,
		max_length=50,
		default="active",
		verbose_name="Статус компани",
		blank=True,
		null=True)
	timestamp = models.DateField(auto_now_add=True, auto_now=False, verbose_name="Дата создания")
	update = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name="Обновлено")
	date_joined = models.DateField(verbose_name="Дата старта", blank=True, null=True)
	company_payment = models.TextField(verbose_name="Тарифы")
	payment_system = models.TextField(verbose_name="Платежные системы", blank=True)
	draft = models.CharField(max_length=50, choices=publish_status, default='draft', verbose_name="Статус публикации")
	company_description = models.TextField(blank=True, null=True, verbose_name="Описание")
	company_img = models.ImageField(upload_to='company_img/',
		blank=True,
		null=True,
		verbose_name="Изображение компании",
		help_text="Для коректного отображения загрузите изображение розмером 768x500")
	video = models.CharField(blank=True, null=True, verbose_name="Сылка на YouTube", max_length=100)

	objects = CompanyManager()

	def status_dict(self):
		return dict(status_company)[self.status]

	def draft_dict(self):
		return dict(publish_status)[self.draft]

	def __str__(self):
		return self.company_name

	def get_absolute_url(self):
		return reverse("company_detail", kwargs={"slug": self.slug})


	@property
	def comments(self):
		instance = self
		qs = Comment.objects.filter_by_instance(instance)
		return qs

	@property
	def get_content_type(self):
		instance = self
		content_type = ContentType.objects.get_for_model(instance.__class__)
		return content_type


	def get_markdown(self):
		company_payment = self.company_payment
		markdown_text = markdown(company_payment)
		return mark_safe(markdown_text)


	class Meta:
		verbose_name_plural = "Компании"
		ordering = ['-user', 'status', '-timestamp']


class ProjectSitemap(Sitemap):
	changefreq = 'never'
	priority = 0.5

	def items(self):
		return Company.objects.active()


class Messages(models.Model):
	user = models.ForeignKey(User, verbose_name="Автор")
	company = models.ForeignKey(Company, verbose_name="Компания")
	message = models.TextField(verbose_name="Сообщения")
	timestamp = models.DateField(auto_now_add=True, verbose_name="Дата создания")


	class Meta:
		verbose_name_plural = 'Сообщения'
		ordering = ['-user', '-timestamp']

	def __unicode__(self):
		return self.user.username

	def __str__(self):
		return self.user.username


class Message_for_payment(models.Model):
	company = models.ForeignKey(Company, verbose_name="Проект")
	value = models.CharField(max_length=100, verbose_name="Сума выплат")
	image = models.ImageField(upload_to="paymet_message/", blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True, verbose_name="Дата публикации")

	class Meta:
		verbose_name_plural = "Сообщения о выплатах"
		verbose_name = "Сообщение о выплате"
		ordering = ['company', '-timestamp']

	def __unicode__(self):
		return self.company.company_name

	def __str__(self):
		return self.company.company_name


def create_slug(instance, new_slug=None):
	slug = slugify(instance.company_name)
	if new_slug is not None:
		slug = new_slug
	qs = Company.objects.filter(slug=slug).order_by("-id")
	exist = qs.exists()
	if exist:
		new_slug = "%s-%s" % (slug, qs.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug

def pre_save_company_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

	if not instance.company_payment:
		html_string = instance.get_markdown()


pre_save.connect(pre_save_company_receiver, sender=Company)
