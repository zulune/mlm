# -*- coding: utf-8 -*-

from django import template
from company.models import Company

register = template.Library()

@register.filter
def monitoring(category, s):
	company = Company.objects.filter(status=s).filter(category_id=category.id)
	c_len = len(company)
	return c_len


@register.filter
def color_tag(s):
	if s == 'active':
		tag = 'badge-success'
	elif s == 'expectation':
		tag = 'badge-info'
	elif s == 'delays':
		tag = 'badge-warning'
	else:
		tag = 'badge-danger'
	return tag


@register.filter
def status_tag(s):
	if s == 'active':
		tag = 'text-success'
	elif s == 'expectation':
		tag = 'text-info'
	elif s == 'delays':
		tag = 'text-warning'
	else:
		tag = 'text-danger'
	return tag

@register.filter
def label_tag(s):
	if s == 'active':
		tag = 'label-success'
	elif s == 'expectation':
		tag = 'label-info'
	elif s == 'delays':
		tag = 'label-warning'
	else:
		tag = 'label-danger'
	return tag
