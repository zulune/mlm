# -*- coding: utf-8 -*-

from django import forms
from company.models import Company
from pagedown.widgets import PagedownWidget
from datetimewidget.widgets import DateWidget, DateTimeWidget

from django.utils.translation import ugettext_lazy as _

class CompanyRegistrationForm(forms.ModelForm):
	# company_payment = forms.CharField(widget=PagedownWidget(show_preview=False), label="Тарифы")
	
	class Meta:
		model = Company
		fields = [
			'company_name', 'company_description', 'company_payment',
			'referal_program', 'payment_system' , 'date_joined',
			'company_url', 'min_value', 'max_value',
			'type_payments', 'plan', 'company_img',
		]

		widgets = {
			'date_joined': DateWidget(attrs={'id': 'id_date_joined', 'placeholder': 'dd.mm.yyyy'}, usel10n=True, bootstrap_version=3)
		}
