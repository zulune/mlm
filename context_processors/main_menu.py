from django.template.context_processors import request

from company.models import Category, status_company, Company, Plan, Message_for_payment
from banners.models import Banner
from blog.models import Blog

def menu(request):
	category_list = Category.objects.all()
	status = status_company
	company = Company.objects.active()
	request_company = Company.objects.filter(draft='draft')
	banner = Banner.objects.active()
	payment = Message_for_payment.objects.all().order_by('-timestamp')[:10]
	plan = Plan.objects.all()
	last_blog = Blog.objects.all().order_by('-date_joined')[:10]
	return {'category_list': category_list,
		'status': status,
		'company_list': company,
		'request_company': request_company,
		'banner': banner,
		'plan': plan,
		'payment': payment,
		'last_blog': last_blog,
	}
