from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib import auth, messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, PasswordChangeForm
from django.views.decorators.csrf import csrf_protect
from django.forms.models import model_to_dict
from django.core.urlresolvers import reverse

from company.models import Company, Messages
from .models import *

from django.contrib.auth.decorators import login_required

from .forms import *
# Create your views here.


def login(request):
    form = AuthenticationForm()
    if request.POST:
        username = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return render_to_response('account/profile.html')
        else:
            print ("Invalid login details: {0}, {1}".format(username, password))
            error_message = 'Invalid password or email'
            template_data = {'error_message': error_message}
            return render_to_response('account/login.html', template_data)
        return render_to_response('account/login.html', {'form': form})

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/login')


def register(request, *args, **kwargs):
    if request.POST:
        form = RegistrationForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            user, created = User.objects.get_or_create(username=username, first_name=first_name, last_name=last_name, email=email)
            if created:
                user.set_password(password1)
                user.save()
            return HttpResponseRedirect('/login')
    else:
        form = RegistrationForm()
    template_data = {'form': form}
        # username = request.POST.get('username', None)
        # email = request.POST.get('email', None)
        # password = request.POST.get('password', None)
        # if email and password:
        #     user, created = User.objects.get_or_create(username=username, email=email)
        #     if created:
        #         user.set_password(password)
        #         user.save()

        #     return HttpResponseRedirect('/login')
    return render(request, 'account/register.html', template_data)


def reset_password(request):
    form = PasswordResetForm()
    return render(request, 'account/reset_password.html', {'form': form})


@login_required
def profile(request):
    message = Messages.objects.all().order_by('-timestamp')
    user = request.user
    user_company = Company.objects.filter(user_id=user.id).order_by('-timestamp')
    template_data = {'message': message, 'user_company': user_company}
    return render(request, 'account/profile.html', template_data)


@login_required
def edit_password(request):
    if request.POST:
        form = PasswordChangeForm(request.POST or None, request.user or None)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Success')
            return HttpResponseRedirect('/accounts/profile/')
        else:
            messages.error(request, 'Error')
    else:
        form = PasswordChangeForm(request.user)
    template_data = {'form': form}
    return render(request, 'account/edit_password.html', template_data)


@login_required
def settings(request):
    user = User.objects.get(pk=request.user.id)
    user_form = UserEditForm(initial=model_to_dict(user))
    profile_form = ProfileForm()
    template_data = {'user_form': user_form, 'profile_form': profile_form}
    return render(request, 'account/settings.html', template_data)

@login_required
def edit_user(request, *args, **kwargs):
    user = User.objects.get(pk=request.user.id)
    form = UserEditForm(request.POST or None, request.user or None)
    if form.is_valid():
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.save()
        return HttpResponseRedirect('/accounts/profile')
    else:
        return HttpResponseRedirect('/accounts/settings')


@login_required
def edit_foto(request, *args, **kwargs):
    form = ProfileForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        profile = Profile.objects.get(user_id=request.user.id)
        profile.avatars = request.FILES['avatars']
        profile.save()
        return HttpResponseRedirect('/accounts/profile')
    else:
        return HttpResponseRedirect('/accounts/settings')
