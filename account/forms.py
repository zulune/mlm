# -*- coding: utf-8 -*-


from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _


from django.utils.safestring import mark_safe

from .models import Profile



ERROR_FLAG = {'required': mark_safe('<button class="btn btn-danger2 btn-xs" type="button"><i class="fa fa-fighter-jet"></i></button>')}


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True,
        widget=forms.TextInput(attrs={'placeholder': 'email@example.com'}),
        label='Email')
    first_name = forms.CharField(required=True, label='Имя')
    last_name = forms.CharField(required=True, label='Фамилия')


    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')


    def clean_email(self):
        email = self.cleaned_data['email']


        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError('Такой адрес электронной почты уже зарегестрирован.')

        # email_qs = User.objects.filter(email=email)
        # if email_qs.exists():
        #     raise forms.ValidationError('Email allready exists')
        # return email




class UserEditForm(forms.ModelForm):
    first_name = forms.CharField(label="Имя", error_messages=ERROR_FLAG, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label="Фамилия", error_messages=ERROR_FLAG, widget=forms.TextInput(attrs={'class': 'form-control'}))
    class Meta:
        model = User
        fields = ("first_name", "last_name")



class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('avatars',)


# class ProfileForm(forms.Form):
#     avatars = forms.ImageField()


class EditPassword(PasswordChangeForm):
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification.")
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2
