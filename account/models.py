# -*- coding: utf-8 -*-


from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
# Create your models here.


def user_directory_path(instance, filename):
	return 'user/user_{0}/{1}'.format(instance.user.id, filename)


class ProfileManager(models.Manager):
	def subscribe_toggle(self, request_user, notification):
		profile_ = Profile.objects.get(user=request_user)
		if notification:
			profile_.notifications = False
		else:
			profile_.notifications = True
		profile_.save()
		return profile_

	def get_receiver(self):
		profile_list = Profile.objects.filter(notifications=True)
		receiver_list = []
		for pl in profile_list:
			receiver_list.append(pl.user.email)
		return receiver_list


class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	notifications = models.BooleanField(default=True)
	avatars = models.ImageField(upload_to=user_directory_path, blank=True, null=True)

	objects = ProfileManager()

	def __unicode__(self):
		return self.user.username

	def __str__(self):
		return self.user.username


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
