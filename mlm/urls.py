"""
mlm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import (
			password_reset,
			password_reset_done,
			password_reset_confirm,
			password_reset_complete,
			password_change,
			password_change_done,
		)
from django.conf import settings
admin.autodiscover()
from django.views.generic import TemplateView
from company.models import ProjectSitemap
from company import views as company
from account import views as account
from blog import views as blog
from chat import views as chat
from banners import views as banner
from django.contrib.auth import views as auth

from django.contrib.sitemaps.views import sitemap

sitemaps = {
    'projects': ProjectSitemap,
}

urlpatterns = [
	# url(r'^home/', company.index, name='index'),
	url(r'^', include('company.urls')),
    # accoun vies
	url(r'^login/', auth.login, {'template_name': 'account/login.html'}, name='login'),
	url(r'^logout/', account.logout, name='logout'),
	url(r'^register/', account.register, name='register'),
	url(r'^accounts/profile/', account.profile, name='profile'),
	url(r'^accounts/password/change/$', password_change, name="password_change"),
	url(r'^accounts/password_change_done', password_change_done, name="password_change_done"),
    url(r'^reset_password/$', password_reset, name="password_reset"),
    url(r'^accounts/password/change/done/$', password_reset_done, name="password_reset_done"),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm, name="password_reset_confirm"),
    url(r'^reset_password/complete/', password_reset_complete, name="password_reset_complete"),
    url(r'^accounts/settings/', account.settings, name="settings"),
    url(r'^accounts/edit_user/', account.edit_user, name="edit_user"),
    url(r'^accounts/edit_foto/', account.edit_foto, name="edit_foto"),
    ########## company views ###################
	# url(r'^company/$', company.company_all, name="company_all"),
	# url(r'^company_detail/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', company.company_detail, name='company_detail'),
	# url(r'^company_filter/(?P<id>[?a-z=A-Z0-9_.-]*)/$', company.company_filter, name='company_filter'),
	# url(r'^company/(?P<status>[?a-z=A-Z0-9_.-]*)/$', company.status_filter, name='status_filter'),
	# url(r'^listing/(?P<id>[?a-z=A-Z0-9_.-]*)/$', company.listing, name='listing'),
	# url(r'^category/(?P<id>[?a-z=A-Z0-9_.-]*)/(?P<status>[?a-z=A-Z0-9_.-]*)/$', company.category_filter, name='category_filter'),
	# url(r'^company_registration/$', company.company_registration, name='company_registration'),
	# url(r'^wizzard_company/$', company.wizzard_company, name="wizzard_company"),
	# url(r'^company/error/$', company.wizzard_pay_error, name='wizzard_pay_error'),
	# url(r'^scam/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', company.scam, name='scam'),
	url(r'^search/', company.search, name="search"),
    # Notification
    url(r'^notification/', include('notifications.urls', namespace="notification")),
    # blog views
    url(r'^blog/', include('blog.urls')),
	# url(r'^blog/', blog.blog, name='blog'),
	# url(r'^blog_single/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', blog.blog_single, name='blog_single'),
	# url(r'^faq/', blog.faq, name='faq'),
    # Chat Views
	url(r'^chat/', chat.chat, name='chat'),
	url(r'^post/', chat.post, name='post'),
	url(r'^messages/', chat.messages, name='messages'),
	# Banner views
	url(r'^banners_registration/$', banner.add_banner, name="add_banner"),
    # robots
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt', content_type="text/plain")),
    # Sitemap
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    # Admin views
	url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# if settings.DEBUG:
#     # static files (images, css, javascript, etc.)
#     urlpatterns += [
#         url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#         	'document_root': settings.MEDIA_ROOT})
#         ]
