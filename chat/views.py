from django.shortcuts import render
from django.http import JsonResponse
from .models import Chat
# Create your views here.

def chat(request):
	c = Chat.objects.all()
	user = request.user
	template_data = {'chat': c, 'user': user}
	return render(request, 'chat/chat.html', template_data)


def post(request):
	if request.POST:
		msg = request.POST.get('msgbox', None)
		c = Chat(user=request.user, message=msg)
		if msg != '':
			c.save()
		return JsonResponse({'msg': msg, 'user': c.user.username})


def messages(request):
	c = Chat.objects.all()
	template_data = {'chat': c}
	return render(request, 'chat/messages.html', template_data)