# -*- coding: utf-8 -*-


from __future__ import unicode_literals

from django.db import models


from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from django.conf import settings
# Create your models here.


class Chat(models.Model):
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
	user = models.ForeignKey(User, verbose_name="Автор")
	message = models.TextField(max_length=200, verbose_name="Сообщения")

	def __unicode__(self):
		return self.user.username