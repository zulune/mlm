from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import LoadBanner
# Create your views here.

def add_banner(request, *args, **kwargs):
	if request.POST:
		form = LoadBanner(request.POST or None, request.FILES or None)
		if form.is_valid():
			post_data = form.cleaned_data
			banner = Banner.objects.create(**post_data)
			return HttpResponseRedirect('/')
	else:
		form = LoadBanner()
	template_data = {'form': form}
	return render(request, 'banner/add_banner.html', template_data)


def wizzard_banner(request):
	template_data = {}
	return render(request, 'banner/wizzard_banner.html')