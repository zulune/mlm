# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import datetime
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save

# Create your models here.

pay_choice = (
	("5", "Верхний"),
	("3", "Центральный"),
	("1", "Боковой")
)
	
class BannerManager(models.Manager):
	def active(self, *args, **kwargs):
		return super(BannerManager, self).filter(continue_date__gt=timezone.now()).filter(active=True)

class Banner(models.Model):
	pay = models.CharField(choices=pay_choice, max_length=50, verbose_name="Баннер")
	site = models.URLField(verbose_name="Сылка на проект")
	email = models.EmailField(verbose_name="Контактный адрес")
	img_url = models.TextField(verbose_name="Сылка", max_length=250)
	update = models.DateField(auto_now=True)
	continue_date = models.DateField(verbose_name="Дата окончания", blank=True, null=True)
	time = models.PositiveIntegerField(verbose_name="Количество дней")
	money = models.CharField(max_length=50, verbose_name="Окончатильная сума")
	active = models.BooleanField(default=False, verbose_name="Статус розмищения")

	objects = BannerManager()

	class Meta:
		verbose_name_plural = "Баннер"
		ordering = ("email", "update", "continue_date")


	def __unicode__(self):
		return self.email


	def get_date(self):
		new_date = self.continue_date
		days = self.time
		newDate = self.update
		new_date = newDate + datetime.timedelta(days=days)
		return new_date


def get_date(instance):
	timeNow = datetime.datetime.now()
	continue_date = instance.continue_date
	time = instance.time
	if time is not 0:
		continue_date = timeNow + datetime.timedelta(days=time)
	else:
		continue_date = timeNow
	return continue_date

def pre_save_date(sender, instance, *args, **kwargs):
	if not instance.continue_date:
		instance.continue_date = get_date(instance)

pre_save.connect(pre_save_date, sender=Banner)


