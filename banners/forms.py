# -*- coding: utf-8 -*-

from django import forms
from .models import Banner


class LoadBanner(forms.ModelForm):
	money = forms.CharField(widget=forms.TextInput(attrs={'readonly': True}), label="К оплате")
	class Meta:
		model = Banner
		fields = ('pay', 'site', 'email', 'img_url', 'time', 'money')