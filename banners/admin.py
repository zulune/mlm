from django.contrib import admin
from . models import *
# Register your models here.

class BannerAdmin(admin.ModelAdmin):
	list_display = ('email', 'site', 'pay', 'active', 'update', 'continue_date')


admin.site.register(Banner, BannerAdmin)
