from django.contrib import admin
from .models import Blog, Faq
from django.db import models
from pagedown.widgets import AdminPagedownWidget
# Register your models here.

class BlogAdmin(admin.ModelAdmin):
    list_display = ['title', 'preview', 'date_joined']
    prepopulated_fields = {'slug': ('title',)}
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget },
    }


admin.site.register(Blog, BlogAdmin)
admin.site.register(Faq)
