from django.shortcuts import render
from django.views.generic import ListView, DetailView
from blog.models import Blog, Faq
# Create your views here.

class BlogView(ListView):
	template_name = 'blog/blog_all.html'

	def get_queryset(self):
		return Blog.objects.all()


class BlogDetailView(DetailView):
	template_name = 'blog/blog_single.html'

	def get_queryset(self):
		return Blog.objects.all()

class FaqView(ListView):
	template_name = 'blog/faq.html'

	def get_queryset(self):
		return Faq.objects.all()


def blog(request):
	blog = Blog.objects.all().order_by('-date_joined')
	template_data = {'blog': blog}
	return render(request, 'blog/blog_all.html', template_data)


def blog_single(request, slug):
	blog = Blog.objects.get(slug=slug)
	template_data = {'blog': blog}
	return render(request, 'blog/blog_single.html', template_data)


def faq(request):
	faq_list = Faq.objects.all()
	template_data = {'faq_list': faq_list}
	return render(request, 'blog/faq.html', template_data)
