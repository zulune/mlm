from django.conf.urls import include, url

from .views import *

urlpatterns = [
    url(r'^$', BlogView.as_view(), name='blog'),
    url(r'^detail/(?P<slug>[?a-z=A-Z0-9_.-]*)/$', BlogDetailView.as_view(), name='blog_single'),
    url(r'^faq/$', FaqView.as_view(), name='faq'),
]
