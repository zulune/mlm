# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save

# Create your models here.

class Blog(models.Model):
	title = models.CharField(max_length=100, verbose_name="Заголовок")
	slug = models.SlugField(unique=True, verbose_name="Имя в браузере", blank=True, null=True)
	preview = models.TextField(verbose_name="Короткое описание", blank=True, null=True)
	body = models.TextField(verbose_name="Контент")
	image = models.ImageField(upload_to='blog/image', verbose_name="Изображение")
	date_joined = models.DateTimeField(auto_now_add=True, verbose_name="Дата публикации")

	def __unicode__(self):
		return self.title

	class Meta:
		verbose_name_plural = "Публикации"
		verbose_name = "Публикацию"


class Faq(models.Model):
	ask = models.TextField(verbose_name="Вопрос")
	answer = models.TextField(verbose_name="Ответ")

	def __unicode__(self):
		return self.ask

	class Meta:
		verbose_name_plural = 'F.A.Q'


# def create_slug(instance, new_slug=None):
# 	slug = slugify(instance.title)
# 	if new_slug is not None:
# 		slug = new_slug
# 	qs = Blog.objects.filter(slug=slug).order_by("-id")
# 	exist = qs.exists()
# 	if exist:
# 		new_slug = "%s-%s" % (slug, qs.first().id)
# 		return create_slug(instance, new_slug=new_slug)
# 	return slug

# def pre_save_blog_receiver(sender, instance, *args, **kwargs):
# 	if not instance.slug:
# 		instance.slug = create_slug(instance)


# pre_save.connect(pre_save_blog_receiver, sender=Blog)